<?php
error_reporting(E_ALL); 
ini_set('display_errors', 1);

include './libs/functions.php';
include './libs/header.php';
include './libs/navigation.php';

echo '<br><br><br>';
echo '<div class="container-fluid">';

if (isset($_GET["n"])) {
    $n = $_GET["n"];
    switch ($n) {
        case "about":
            require './pages/about.php';
            break;
        case "saas":
            require './pages/saas.php';
            break;
        case "home":
            require './pages/first.php';
            break;
        case "team":
            require './pages/team.php';
            break;
        case "acc":
            require './pages/acc.php';
            break;
        case "topology":
            require './pages/topology.php';
            break;
        case "itdotcom":
            require './pages/itdotcom.php';
            break;
        case "test":
            require './pages/test.php';
            break;
            
            default: require './pages/first.php';
    }
}
else {require './pages/first.php';}
echo '</div>';
include './libs/footer.php';
?>
