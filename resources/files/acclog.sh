#!/bin/bash

#####ACCLOG SCRIPT ALAN SPILLANE


FILENAME=$1


#Variables---------------------------------------------------------------------------------------------------------------------------------------------

#####This will search for the ACC version and output it

version=$(grep Starting /root/test/$1.log | tail -1 | awk '{ print $12 }')

#####This will search for the .NET Framework Version and output it

framework=$(grep .NET /root/test/$1.log | tail -1 | awk '{ print $12 }')

#####This will search for the ACC server hostname

acchost=$(grep Starting /root/test/$1.log | tail -1 | awk '{ print $3 }')

#####This will search for the console server URL

console=$(grep "Checking for update with" /root/test/$1.log | tail -1 | awk '{ print $13 }') 

#####This will count the amount of Error code:49 instances throughout the log file

fortynine=$(cat * | grep -c "Error code:49" /root/test/$1.log)
fortynine2=$(grep "Error code:49" /root/test/$1.log | tail -5)

#####This will search for last instance of the Error code:49

process=$(grep "Error code:49" /root/test/$1.log | tail -1 | awk '{ print $4}')

#####Using the process information from the last instance of the Error code:49 this will search for the bind user account that caused the failed login 

#serviceaccount1=$(grep -B 1 $process /root/test/$1.log | awk '{ print $17 }')
#serviceaccount=$(grep $process /root/test/$1.log | grep -oE "Attempting .*'" | awk '{ print $9}')
serviceaccount=$(grep $process /root/test/$1.log | grep -oE "Attempting .*'" | awk -F\' '{ print $(NF-1)}')


#####This will count the amount of Error code:81 instances throughout the log file

eightyone=$(cat * | grep -c "Error code:81" /root/test/$1.log) 
eightyone2=$(grep "Error code:81" /root/test/$1.log | tail -5)


#####This will count the amount of "Error code:87" instances throughout the log file

eightyseven=$(cat * | grep -c "Error code:87" /root/test/$1.log)
eightyseven2=$(grep "Error code:87" /root/test/$1.log | tail -5)

#####This will count the amount of SSL/TLS errors in the log file

trust=$(cat * | grep -c "Could not establish trust relationship for the SSL/TLS secure channel" /root/test/$1.log)

#####This will search for out of date ACC's

#update1=$(grep "The component may be out-of-date" /root/test/$1.log | tail -1)
update=$(grep out-of-date /root/test/$1.log | tail -1) 


######This Search for the date in the last string in the file and out puts all string from that date will use this to filter results down to last day

lastupdate=$(tail -1 /root/test/$1.log | awk '{print $1}')

lastupdate2=$(grep $lastupdate /root/test/$1.log)


userfilter=$(grep "User Member Search Filter:" /root/test/$1.log | tail -1 | awk '{ print $13 }')

groupfilter=$(grep "Group Member Search Filter:" /root/test/$1.log | tail -1 | awk '{ print $13 }')



#sAMAccountName=$(grep "sAMAccountName:" /root/test/$1.log

#####List all Base DN's listed in the logs once

basedn=$(grep "at BaseDN:" /root/test/$1.log | awk '{print $(NF -1)}' | sort | uniq)

#####Get AWCM ip address

awcm=$(grep  "No connection could be made because the target machine actively refused it" /root/test/$1.log | grep :2001 | awk '{ print $NF }' | sort | uniq)


error=$(grep Error $1.log)

#Output-----------------------------------------------------------------------------------------------------------------------------------------------------



echo "The Version of ACC is: " $version
echo	
echo "The .NET Framework version is: " $framework
echo
echo "The ACC Server hostname is: " $acchost
echo
echo "The Console URL is: " $console
echo
echo "The instances of an Error Code:49 in this log file: " $fortynine
echo
echo "The instances of an Error Code:81 in this log file: " $eightyone
echo
echo "The instances of SSL/TLS secure channel errors in this log file: " $trust
echo
if [ -n "${serviceaccount}" ]; then
echo "The last failed bind attempt was with the bind user: " $serviceaccount
fi
#echo $lastupdate2
#echo

#####if $update is set echo $basedn

if [ -n "${update}" ]; then
echo $update
echo "Update the Cloud Connector to the latest version"
fi

#####if $userfilter is se echo $userfilter

if [ -n "${userfilter}" ]; then
echo
echo $userfilter

fi

#####if $groupfilter is se echo $groupfilter

if [ -n "${groupfilter}" ]; then
echo
echo $groupfilter
fi




#####if $basedn is set echo $basedn

if [ -n "${basedn}" ]; then
echo
echo $basedn
fi

 
#####if $awcm is set echo $awcm
echo

if [ -n "${awcm}" ]; then
echo "Connection issues with the AWCM, the ipaddress for the AWCM is" $awcm
echo "Try browsing to the AWCM URL from the ACC and if you see a certificate error see https://airwatch.it.com/display-solution/594?fromSearchPage=true for details on how to replace the AWCM certificate"
echo
echo "------------------------------------------------------------------------------------------------------------------------------------------------------------"
fi

#####if $fortynine2 is set echo $fortynine2
if [ -n "${fortynine2}" ]; then
echo "------------------------------------------------------------------------------------------------------------------------------------------------------------"
echo Most recent Error Code:49 log entries
echo "------------------------------------------------------------------------------------------------------------------------------------------------------------"
echo
echo $fortynine2
echo
echo "------------------------------------------------------------------------------------------------------------------------------------------------------------"
echo
echo "If you are seeing this error in the logs go to the Directory Services Settings page in the Console and hit the Test Connection button and you should see the same Error Code:49 error thrown here(please note that the Cloud Connector connection should test successfully even though this is failing). You can resolve this issue by verifying that the Bind Users password hasn't expired in Active Directory or else resetting the Bind Users password in Active Directory and updating it within the console also under the Directory Services settings.(NOTE: This error can also be thrown in the logs if an Admin types in their Username and Password incorrectly)."
echo
echo "------------------------------------------------------------------------------------------------------------------------------------------------------------"
echo
echo
echo
fi

#####if $eightyseven2 is set echo $eightyseven2
if [ -n "${eightyseven2}" ]; then
echo "------------------------------------------------------------------------------------------------------------------------------------------------------------"
echo Most recent Error Code:87 log entries
echo "------------------------------------------------------------------------------------------------------------------------------------------------------------"
echo
echo $eightyseven2
echo
echo "------------------------------------------------------------------------------------------------------------------------------------------------------------"
echo
echo "If you are seeing this error when pressing the Test Connection button for the Cloud Connector you can resolve it by making sure that the User and Group search filters are correct in the console under the Directory services settings User tab. The standard settings the majority of customers use are User Object Class: person and User Search Filter: (&(objectCategory=person)(sAMAccountName={EnrollmentUser}))"
echo
echo "------------------------------------------------------------------------------------------------------------------------------------------------------------"
echo
echo
echo
fi

#####if $eightyone2 is set echo $eightyone2
if [ -n "${eightyone2}" ]; then
echo "------------------------------------------------------------------------------------------------------------------------------------------------------------"
echo Most recent Error Code:81 log entries
echo "------------------------------------------------------------------------------------------------------------------------------------------------------------"
echo
echo $eightyone2
echo
echo "------------------------------------------------------------------------------------------------------------------------------------------------------------"
echo
echo "If you are seeing this Error when pressing the Test Connection button for the Cloud Connector or throughout the logs it typically points to a network issue between the Cloud Connector server and the Domain Controller. To verify if this is the case go to the Directory Services settings page in the Console and make note of the Domain Controller in the Server field and also the port number in the Port field(this will either be 389(Non-SSL), 636(SSL), 3268(Global Catalog, Non-SSL) or 3269(Global Catalog, SSL). Then from the ACC server try to Telnet to the Domain Controller over this port, it is likely that it is blocked and the customer will have to resolve this by opening up the port on their network to the ACC."
echo
echo "------------------------------------------------------------------------------------------------------------------------------------------------------------"
echo
echo
echo
fi







