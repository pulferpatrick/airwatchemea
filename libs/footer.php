<footer class="page-footer unique-color center-on-small-only wow slideInUp">
 <div class="container-fluid">
  <div class="social-section text-center wow bounceInUp">
  </div>
 </div>
 <div class="footer-copyright text-center rgba-black-light">
  <div class="container-fluid">
   <a href="mailto:patrick@ppulfer.cloud" style="text-decoration:none;">© 2016 Copyright: AirWatch EMEA |  Designed by: Patrick Pulfer & Deirdre O Brien</a>
  </div>
 </div>
</footer>

<script type="text/javascript" src="https://pweb.solutions/repositories/mdb420/js/jquery-3.1.1.min.js"></script>	
<script type="text/javascript" src="https://pweb.solutions/repositories/mdb420/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://pweb.solutions/repositories/mdb420/js/mdb.js"></script>
<script src="https://use.fontawesome.com/9d0c79ea6b.js"></script>

</body>
</html>