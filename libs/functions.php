<?php

function getVersion($url){
	$server_url = 'https://'.$url.'/AirWatch/AwBase/About';
	$content = file_get_contents($server_url);
	$first_step = explode( '<span>Version:</span><strong>' , $content );
	$second_step = explode("</strong>" , $first_step[1] );
	return $second_step[0];
}

?>