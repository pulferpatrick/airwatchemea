<nav class="navbar navbar-fixed-top scrolling-navbar navbar-dark unique-color">
    <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#collapseEx2"><i class="fa fa-bars"></i></button>
    <div class="container">
        <div class="collapse navbar-toggleable-xs" id="collapseEx2">
            <ul class="nav navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="?n=home"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;AirWatch Tech Hub</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?n=saas"><i class="fa fa-server" aria-hidden="true"></i>EMEA SaaS</a>
                </li>
                <li class="nav-item btn-group">
                    <a class="nav-link dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench" aria-hidden="true"></i>AirWatch In-house Tech Tools</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <a class="dropdown-item" target="_blank" href="https://www.pweb.solutions/tools/adb"><i class="fa fa-android" aria-hidden="true"></i>&nbsp;&nbsp;Android Logcat analyzer</a>
                        <a class="dropdown-item" href="?n=acc"><i class="fa fa-file-code-o" aria-hidden="true"></i>&nbsp;&nbsp;ACC Errors analyzer script</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-book" aria-hidden="true"></i>Training (Comming Soon)</a>
                </li>
                <li class="nav-item btn-group">
                    <a class="nav-link dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-link" aria-hidden="true"></i>Useful Links</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <a class="dropdown-item" target="_blank" href="file://dfs-prod-2/departments/Technical%20Support/AirWatch/"><i class="fa fa-windows" aria-hidden="true"></i>&nbsp;SR Clinics</a>
                        <a class="dropdown-item" target="_blank" href="https://airwatch.zendesk.com/agent/dashboard"><img src="https://d1eipm3vz40hy0.cloudfront.net/images/p-support-overview/support-shape.svg" width="12%"></img>&nbsp;Zendesk</a>
                        <a class="dropdown-item" target="_blank" href="https://jira01.airwatchdev.com/jira/secure/Dashboard.jspa"><img src="https://www.samanage.com/wp-content/uploads/2014/08/jira-icon.png" width="10%"></img>&nbsp;Jira (AW)</a>
                        <a class="dropdown-item" target="_blank" href="https://awsw.air-watch.com/saasutilities/saasaccess"><img src="./resources/airwatch_icon1.png" width="15%"></img>&nbsp;SaaSWatch</a>
                        <a class="dropdown-item" target="_blank" href="https://resources.air-watch.com/"><img src="./resources/airwatch_icon2.png" width="15%"></img>&nbsp;Resources</a>
                        <a class="dropdown-item" target="_blank" href="http://qevel.com/v1.6/airwatch.php?topology"><i class="fa fa-map-o" aria-hidden="true"></i>&nbsp;&nbsp;AirWatch Topology</a>
                        <a class="dropdown-item" target="_blank" href="https://airwatch.it.com"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;AirWatch IT.com</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?n=team"><i class="fa fa-question-circle" aria-hidden="true"></i>About us</a>
                </li>
            </ul>
        </div>
    </div>
</nav>