<div class="container">

<div class="card">
    <div class="text-md-center">
        <img class="img-fluid" src="https://www.idaten.ne.jp/portal/page/out/DIS/vmware/landing/img/colummn007-1.png" width="50%">
    </div>
    <div class="card-block">
        <h4 class="card-title" style="color: #3F729B;">AirWatch Cloud Connector Error Check Script</h4>
        <p class="card-text">This tool will allow the user to analyze common errors with AirWatch Cloud Connector and provide you with it's feedback.
        <p class="card-text">To use this script, please perform the following:</p>
        <p class="card-text">
            <ol>
            <li>On your linux machine, navigate to the AirWatch folder by using the CD command. Example cd c:\airwatch\</li>
            <li>Download the script file via the following command: <mark>wget "http://10.27.132.140/resources/files/acclog.sh"</mark></li>
            <li>Give execute permission to your script: <mark>chmod +x acclog.sh</mark></li>
            <li>Run the script: <mark>acclog.sh</mark></li>
            </ol>
        </p>
        <a href="http://10.27.132.140/resources/files/acclog.sh" target="_blank" class="btn btn-primary">Download</a>
    </div>
</div>
<br><br><br><br><br><br><br><br>

</div>