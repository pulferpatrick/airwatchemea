<br>
<div class="container-fluid">

<div class="row">
  <div class="col-md-12">
    <h2><font color="#3F729B">EMEA SaaS Live Environments Information</font></h2>
    <br>
  </div>
</div>

<div class="row">

<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 1 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
<div class="col-md-6">
  <div class="card">
    <div class="card-block">
      <h4 class="card-header white-text unique-color">Shared SaaS</h4>
      <table class="table">
        <thead>
          <tr>
            <th>Server</th><th>Console Version</th>
          </tr>
        </thead>
        <tbody>
          <tr><td>CN556 (DE)</td><td><?php echo (getVersion('cn556.awmdm.com')); ?></td></tr>
          <tr><td>CN763</td><td><?php echo (getVersion('cn763.awmdm.com')); ?></td></tr>
          <tr><td>CN137</td><td><?php echo (getVersion('cn137.awmdm.com')); ?></td></tr>
          <tr><td>CN706</td><td><?php echo (getVersion('cn706.awmdm.com')); ?></td></tr>
          <tr><td>CN801</td><td><?php echo (getVersion('cn801.awmdm.com')); ?></td></tr>
          <tr><td>CN902</td><td><?php echo (getVersion('cn902.awmdm.com')); ?></td></tr>
          <tr><td>CN32</td><td><?php echo (getVersion('cn32.airwatchportals.com')); ?></td></tr>
          <tr><td>CN37</td><td><?php echo (getVersion('cn37.airwatchportals.com')); ?></td></tr>
          <tr><td>CN503 (UK)</td><td><?php echo (getVersion('cn503.awmdm.co.uk')); ?></td></tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 2 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
<div class="col-md-6">
  <div class="card">
    <div class="card-block">
      <h4 class="card-header white-text unique-color">Dedicated SaaS</h4>
      <table class="table">
        <thead>
          <tr>
            <th width="150">Server&nbsp;</th><th width="160">Console Version</th><th width="220">Customer</th>
          </tr>
        </thead>
        <tbody>
          <tr><td>CN613 (DE)</td><td><?php echo (getVersion('msadpesi.awmdm.com')); ?></td><td>ADP Europe</td></tr>
          <tr><td>CN711 (DE)</td><td><?php echo (getVersion('emea-mdm.dimensiondata.com')); ?></td><td>Dimension Data Europe</td></tr>
          <tr><td>CN1008 (UK)</td><td><?php echo (getVersion('mdmadmin-siee.awmdm.com')); ?></td><td>Sony Europe (Prod)</td></tr>
          <tr><td>CN1005 (UK)</td><td><?php echo (getVersion('mdmadmin-siee-uat.awmdm.com')); ?></td><td>Sony Europe (UAT)</td></tr>
          <tr><td>CN511 (DE)</td><td><?php echo (getVersion('mdm.o2business.de')); ?></td><td>Telefonica Germany</td></tr>
          <tr><td>CN593 (DE)</td><td><?php echo (getVersion('adminde.capgemini-mm.com')); ?></td><td>Ericsson Germany</td></tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div><!-- Row -->
</div><!-- Container -->