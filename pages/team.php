<div class="container">

<div class="row">
  <div class="col-md-12 text-md-center">
    <h1><font color="#3F729B">Team AirWatch Cork</font></h1>
    <div class="text-md-center">
        <img src="./resources/team_spelling2.png" width="10%">
    </div>
    <br>
  </div>
</div>

<div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img src="./resources/team5.jpg" alt="First slide">
        </div>
        <div class="carousel-item">
            <img src="./resources/team1.jpg" alt="Second slide">
        </div>
        <div class="carousel-item">
            <img src="./resources/team3.jpg" alt="Third slide">
        </div>
        <div class="carousel-item">
            <img src="./resources/team4.jpg" alt="Third slide">
        </div>
    </div>

    <a class="left carousel-control" href="#carousel-thumb" role="button" data-slide="prev">
        <span class="icon-prev" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-thumb" role="button" data-slide="next">
        <span class="icon-next" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>

    <ol class="carousel-indicators">
        <li data-target="#carousel-thumb" data-slide-to="0" class="active"> <img src="./resources/team5.jpg" class="img-fluid"></li>
        <li data-target="#carousel-thumb" data-slide-to="1"><img src="./resources/team1.jpg" class="img-fluid"></li>
        <li data-target="#carousel-thumb" data-slide-to="2"><img src="./resources/team3.jpg" class="img-fluid"></li>
        <li data-target="#carousel-thumb" data-slide-to="3"><img src="./resources/team4.jpg" class="img-fluid"></li>
    </ol>
</div>


</div>