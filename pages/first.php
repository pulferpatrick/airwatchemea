<br>
<div class="container"><div class="row">

<div class="col-md-9">
<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 8 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
<div class="card">
    <div class="card-block">
        <h4 class="card-title">Time to Resolution - Enterprise & Production</h4>
        <img class="img-fluid" src="./resources/ttr_prod.png">
    </div>
</div>

<div class="card">
    <div class="card-block">
        <h4 class="card-title">Solved Cases - Enterprise & Production</h4>
        <img class="img-fluid" src="./resources/solved_prod.png" alt="Logo needed!">
    </div>
</div>

<div class="card">
    <div class="card-block">
        <h4 class="card-title">Time to Resolution - Basic Team</h4>
        <img class="img-fluid" src="./resources/ttr_basic.png" alt="Logo needed!">
    </div>
</div>

<div class="card">
    <div class="card-block">
        <h4 class="card-title">Solved Cases - Basic Team</h4>
        <img class="img-fluid" src="./resources/solved_basic.png" alt="Logo needed!">
    </div>
</div>

<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 8 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
</div>

<div class="col-md-3">
<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 3 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
<div class="card">

    <img class="img-fluid" src="./resources/logo_dee3.png">
    <div class="card-block">
        <div class="card-title text-center" style="color: #3F729B;"><b>What is AirWatch IT Hub?</b></div>
        <p class="card-text">
            AirWatch IT Hub is a central location that allows our team to share ideas, upload helpful information and tools. 
            <br><br>
            This will support our team in efficient day-to-day troubleshooting of customer issues which in the long term helps our customers.
        </p>
        <p class="card-text">
        <a href="?n=team"><img src="./resources/team_spelling.jpg" class="img-fluid" /></a>
        Click <a href="?n=team">here</a> for background on our wonderful team.
        </p>
    </div>
</div>
<div class="card">
    <img class="img-fluid" src="./resources/aw1.jpg">
    <div class="card-block">
        <h4 class="card-title">Getting Ready for AW 9</h4>
        <p class="card-text">AirWatch 9.0 is now available!  You can view the latest SaaS deployment schedule <a target="_blank" href="https://resources.air-watch.com/view/hx2qy5njxhdrwmd7vz8x/en">here</a>. Subscribe to the following page to stay up to date with the latest information about this release, including technical bulletins, blog posts, known issues, release notes, and documentation.</p>
        <p class="card-text"><a target="_blank" href="https://support.air-watch.com/articles/102787467">Click here for more info!</a></p>
    </div>
</div>
<div class="card">
    <div class="card-block">
        <h4 class="card-title">The AirWatch Story</h4>
        <p class="card-text"><a target="_blank" href="http://www.vmware.com/uk/company/acquisitions/airwatch.html">VMWaire & AirWatch</a></p>
    </div>
</div>
<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 3 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
</div>

</div></div>