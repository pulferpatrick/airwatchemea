<?php
error_reporting(E_ALL); 
ini_set('display_errors', 1);

include '../libs/functions.php';
include '../libs/header.php';

$url =  "//{$_SERVER['HTTP_HOST']}";
$url = "http:".$url;
?>

<div class="container-fluid unique-color text-white text-xs-center" style="padding: 10px;">
	<a style="color: white;" href="<?php echo $url;?>"> <h2>Visit http://10.27.132.140/ for team page!</h2></a>
</div>

<table width="100%" height="70%">
	<tr>
		<td width="50%" align="center" style="padding: 10px;">
			<div class="card">
    			<div class="card-block">
        			<h4 class="card-title">Time to Resolution - Enterprise & Production</h4>
        			<img class="img-fluid" src="../resources/ttr_prod.png" width="75%">
    			</div>
			</div>
		</td>
		<td width="50%" align="center" style="padding: 10px;">
			<div class="card">
    			<div class="card-block">
        			<h4 class="card-title">Solved Cases - Enterprise & Production</h4>
        			<img class="img-fluid" src="../resources/solved_prod.png" width="80%">
    			</div>
			</div>
		</td>
	</tr>
	<tr>
		<td width="50%" align="center" style="padding: 10px;">
			<div class="card" width="99%">
    			<div class="card-block">
      				<h4 class="card-title">Time to Resolution - Basic Team</h4>
    		    	<img class="img-fluid" src="../resources/ttr_basic.png" width="75%">
    			</div>
			</div>
		</td>
		<td width="50%" align="center" style="padding: 10px;">
			<div class="card">
    			<div class="card-block">
        			<h4 class="card-title">Solved Cases - Basic Team</h4>
        			<img class="img-fluid" src="../resources/solved_basic.png" width="80%">
    			</div>
			</div>
		</td>
	</tr>
</table>
<br><br><br><br><br>

<?php
include '../libs/footer.php';
?>